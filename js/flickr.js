//AJAX and APIs
// Se ha creado un código para acceder al API de Flicker y poder ver las imagenes correspondientes a los tags de dog, cat y moose al hacer click en el respectivo botón

$(document).ready(function(){ //hace que el código de JS espere a que se cargue todo el HTML
  $('button').click(function(){
    $("button").removeClass("selected");
    $(this).addClass("selected");
    
    var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
    var animal = $(this).text(); //this refers to the button that is clicked and save the text of it (cat, dog, moose)
    var flickrOptions = {
      tags: animal,
      format: "json"
    };
    
    function displayPhotos(data){
      var photoHTML = '<ul>';
      $.each(data.items, function(i, photo){
        photoHTML += '<li class="grid-25 tablet-grid-50">';
        photoHTML += '<a href="' + photo.link + '" class="image">';
        photoHTML += '<igm src="' + photo.media.m + '"> </a></li>';
      });
      photoHTML += '</ul>';
      $('#photos').html(photoHTML);
    };
    
    $.getJSON(flickerAPI, flickrOptions, displayPhotos); // (url, data, callback function)
  });//End button
  
}); //End ready